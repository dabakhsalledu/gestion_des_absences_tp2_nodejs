//import database
const mongoose = require('mongoose');
const presenceSchema = new mongoose.Schema({
    datetime: {
        type: Date
    }
});
const etudiantShema = new mongoose.Schema({
    matricule: {
        type: String,

    },
    nom: {
        type: String,

    },
    prenom: {
        type: String,

    },
    presence: [presenceSchema]
});

module.exports = mongoose.model('etudiant', etudiantShema);