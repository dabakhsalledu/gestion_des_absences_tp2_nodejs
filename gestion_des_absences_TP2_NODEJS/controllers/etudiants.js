//import collection
const Etudiant = require('../models/etudiant');
//si l'authentification n'est pas valide
const Isauthennotvalid = (u, p) => {
    if (u != "jbeauchemin@teccart.online") return true;
    if (p != "AAAaaa111") return true;
    return false;
}
//si le nom n'est pas valide 
const Isnamenotvalid = (name) => {
    if (name == "") return true
    if (name === undefined) return true
    return false
}
//si le prenom n'est pas valide 
const Isprenomnotvalid = (pnom) => {
    if (pnom == "") return true
    if (pnom === undefined) return true
    return false
}
//si le matricule n'est pas valide
const Ismatriculenotvalid = (matri) => {
    if (matri === undefined) return true
    if (matri == "") return true
    return false
}
//@ROUTE GET/students
//@DESC get all students
//ACCESS Public
exports.getStudents = async (req, res) => {
    const { user, pwd } = req.body;
    //on verifie si l'authentification est valide 
    if (Isauthennotvalid(user, pwd)) {
        return res.send({ 'success': false, 'message ': 'non autorisé' });
    }
    //on affiche tout les informations des l'etudiants sauf sa __v et sa presence
    const etud = await Etudiant.find().select('-__v -presence');
    res.send({ 'success': true, 'data': etud });
}
//@ROUTE POST/student
//@DESC create one student
//ACCESS Public
exports.createoneStudent = async (req, res) => {
    const { matricule, nom, prenom, user, pwd } = req.body;
    //on verifie si l'authentification est valide 
    if (Isauthennotvalid(user, pwd)) return res.send({ 'success': false, 'message ': 'non autorisé' });
    //on verifie  si le nom n'est pas valide
    if (Isnamenotvalid(nom)) return res.send({ 'success': false, 'message ': 'name is not valid' });
    //on verifie si le prenom n'est pas valide
    if (Isprenomnotvalid(prenom)) return res.send({ 'success': false, 'message ': 'lastname is not valid' });
    //on verifie si 
    if (Ismatriculenotvalid(matricule)) return res.send({ 'success': false, 'message ': 'matricule is not valid' });
    const filters = { 'matricule': matricule };
    const etud = await Etudiant.findOne(filters);
    //on verifie si il y a un etudiant avec le meme matricule
    if (etud) {
        return res.send({ 'success': false, 'message ': `il existe deja un étudiant avec le matricule ${matricule} ` });
    }
    //on cree un etudiant dont les informations sont valide
    await Etudiant.create({ matricule: matricule, nom: nom, prenom: prenom });
    res.send({ 'success': true, 'message ': 'Étudiant crée' });
}
//@ROUTE DELETE/student/id
//@DESC delete one student
//@ACCES Public
exports.deleteoneStudent = async (req, res) => {
    try {
        const { id } = req.params;
        const { user, pwd } = req.body;
        if (Isauthennotvalid(user, pwd)) {
            return res.send({ 'success': false, 'message ': 'non autorisé' });
        }
        const filters = { '_id': id };
        //on cherche un le id de params dans notre bd
        const etud = await Etudiant.findOne(filters);
        //on verifie si le id est valide 
        if (etud === null) {
            return res.send({ 'success': false, 'message ': `Invalid id: ${id} ` });
        }
        //on supprime l'etudiant avec le id spécifique
        await Etudiant.deleteOne(filters);
        res.send({ 'success': false, 'message ': 'Étudiant supprimé ' });
    } catch (error) {
        res.send(error);
    }
}
//ROUTE PUT/student/id
//DESC update one student
//ACCESS Public
exports.updateoneStudent = async (req, res) => {
    try {
        const { id } = req.params;
        const { matricule, nom, prenom, user, pwd } = req.body;
        if (Isauthennotvalid(user, pwd)) return res.send({ 'success': false, 'message ': 'non autorisé' });
        if (Isnamenotvalid(nom)) return res.send({ 'success': false, 'message ': 'name is not valid' });
        if (Isprenomnotvalid(prenom)) return res.send({ 'success': false, 'message ': 'lastname is not valid' });
        if (Ismatriculenotvalid(matricule)) return res.send({ 'success': false, 'message ': 'matricule is not valid' });
        //on recupere le matricule envoyé 
        const matri = await Etudiant.findOne({ 'matricule': matricule });
        //on verifie que le matricule n'existe pas dans la bd
        if (matri !== null) return res.send({ 'success': false, 'message ': `il existe deja un étudiant avec le matricule ${matricule} ` });
        const filters = { '_id': id };
        //on recherche l'etudiant avec le id specifique
        const etud = await Etudiant.findOne(filters);
        //on modifie l'etudiant trouvé avec ce id dans la bd
        if (etud) {
            await Etudiant.updateOne(filters, { matricule: matricule, nom: nom, prenom: prenom });
            return res.send({ 'success': true, 'message ': 'Étudiant modifié' });
        }
        return res.send({ 'success': false, 'message ': `Invalid id: ${id} ` });
    } catch (error) {
        res.send(error)
    }
}
//ROUTE POST/presence
//DESC create one presence of student
//ACCESS Public
exports.createonepresencestudent = async (req, res) => {
    const { matricule } = req.body;
    const presence = [{ datetime: new Date(Date.now() - 4 * 60 * 60 * 1000) }];
    const filters = { matricule: matricule };
    const etud = await Etudiant.findOne(filters);
    //si on trouve l'etudiant avecs
    if (etud) {
        await Etudiant.updateOne(filters, { presence: presence });

        return res.send({ 'success': true, 'message ': 'présence prise' });
    }
    return res.send({ 'success': false, 'message ': `il n'existe pas un étudiant avec le matricule ${matricule} ` });
}
//ROUTE GET/student/id
//DESC get one student whit her presence
//ACCESS Public
exports.getonestudent = async (req, res) => {
    try {
        const { id } = req.params;
        const { user, pwd } = req.body;
        if (Isauthennotvalid(user, pwd)) {
            return res.send({ 'success': false, 'message ': 'non autorisé' });
        }
        const filters = { '_id': id };
        const etud = await Etudiant.findOne(filters);
        if (etud === null) {
            return res.send({ 'success': false, 'message ': `Invalid id: ${id} ` });
        }
        const etudiant = await Etudiant.findOne(filters).select('-__v');
        res.send({ 'success': true, 'data': etudiant });
    } catch (error) {
        res.send(error);
    }
}