//external import
const { Router } = require('express');
const { createonepresencestudent } = require('../controllers/etudiants')
const router = Router({ mergeParams: true });
router.route('/').post(createonepresencestudent);
module.exports = router;
