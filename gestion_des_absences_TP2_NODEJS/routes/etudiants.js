//external import
const { Router } = require('express');
const { getStudents, createoneStudent, updateoneStudent, deleteoneStudent, getonestudent } = require('../controllers/etudiants')
const router = Router({ mergeParams: true });
router.route('/').get(getStudents).post(createoneStudent);
router.route('/:id').get(getonestudent).put(updateoneStudent).delete(deleteoneStudent);
module.exports = router;
